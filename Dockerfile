FROM python:3.7.9-buster

WORKDIR /app/

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY entrypoint.sh .
COPY wait-for-it.sh .
COPY application application