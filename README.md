# Application peer server
> Для управления peer-каналами

### Деплой.
1. Создать database.env по шаблону database.template.env c параметрами БД. 
2. Прописать параметры, при необходимости в app/settings/production.py.   
3. docker-compose build.
4. docker-compose up -d.
5. Для создания пользователя (внутри контейнера приложения): flask adduser USERNAME PASSWORD.

### Деплой на серверах.
1. На локалхосте `bash deploy_local.sh`.
2. На проде хостинга `bash deploy_hosting.sh`.

### Сборка образа для докер-контейнеров.

Запуск из папки peer-server
`docker build --no-cache -t fastmeet-peer-server -f deploy/production/Dockerfile .`

