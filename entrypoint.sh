#!/bin/sh

##################################################################
# Script for starting application
##################################################################/

# migrate
echo '';
echo 'Start migrate:';
cd application
flask db upgrade
cd ../

# application
echo '';
echo 'Start app:';
python application/main.py