##################################################################
# Скрипт запуска peer-сервера для webrtc
##################################################################

#!/bin/bash

port=$1;
if [ -z $port ]; then
	port=9005;
fi
#######################

echo '';
echo '--- 1. Процессы:';
docker ps | grep 'peer-'

echo '';
echo '--- 2. Запускаем сервер:';
docker run  --name fastmeet-peer-server-$port -p $port:9000 -d fastmeet-peer-server
sleep 3;

echo '';
echo '--- 3. Процессы:';
docker ps | grep 'peer-'
