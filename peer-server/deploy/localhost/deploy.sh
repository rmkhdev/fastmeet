##################################################################
# Скрипт запуска peer-сервера для webrtc
##################################################################

#!/bin/bash

port=$1;
if [ -z $port ]; then
	port=9005;
fi
#######################

echo '';
echo '--- 1. Останавливаем, чистим:';
container=`docker ps -a -q --filter="name=fastmeet-peer-server"`;
docker stop $container;
docker rm $container;
sleep 3;

echo '';
echo '--- 2. Процессы:';
docker ps | grep 'peer-'

echo '';
echo '--- 3. Пересобираем образ:';
docker build --no-cache -t fastmeet-peer-server -f deploy/localhost/Dockerfile .

echo '';
echo '--- 4. Запускаем сервер:';
docker run  --name fastmeet-peer-server -p $port:9000 -d fastmeet-peer-server
sleep 3;

echo '';
echo '--- 5. Процессы:';
docker ps | grep 'peer-'
