from flask import Blueprint, jsonify, make_response, request, Response, session
from application.classes import Peer
from werkzeug.security import check_password_hash
from flask_login import login_user, logout_user, login_required, current_user, fresh_login_required
from application.quiries.users import query_authentication
from application.decorators import check_allow


api_app = Blueprint('api', __name__, url_prefix='/api')

"""
######################################################
AUTHENTICATION
######################################################
"""


@api_app.route('/me', methods=['GET'])
@check_allow
@login_required
def index_view():
    result = {
        'id': current_user.id,
        'name': current_user.name,
        'username': current_user.username,
    }

    return jsonify(result)


@api_app.route('/login', methods=['POST'])
@check_allow
def login_view():
    params = request.get_json()

    username = params.get('username')
    password = params.get('password')

    if username is not None and password is not None:
        session.permanent = True
        user = query_authentication(username)

        if user is not None and check_password_hash(user.password, password):
            login_user(user, remember=False)
            return Response(status=200)

        return Response(status=401)

    return Response(status=400)


@api_app.route('/logout', methods=['POST'])
@check_allow
@login_required
def logout_view():
    logout_user()

    return Response(status=200)


@api_app.route('/check', methods=['GET'])
@check_allow
@login_required
def check_view():
    user = query_authentication(current_user.username)

    if user is not None:
        return make_response(jsonify(user.get_data()), 200)

    return Response(status=400)



"""
######################################################
CONTAINERS
######################################################
"""


@api_app.route('/list', methods=['GET'])
@check_allow
# @login_required
def list_view():
    peer = Peer()
    result = peer.get_all()

    data = dict()
    if result['returncode'] == 0:
        data['result'] = True
        data['data'] = result['data']

        return make_response(jsonify(data), 200)

    else:
        data['result'] = False
        data['data'] = result['data']
        return make_response(jsonify(data), 400)


@api_app.route('/get/<int:port>', methods=['GET'])
@check_allow
# @login_required
def get_view(port):
    peer = Peer()
    result = peer.get(port)

    data = dict()

    if result['returncode'] == 0:
        data['result'] = True
        if len(result['data']) > 0:
            data['data'] = result['data'][0]
        else:
            data['data'] = None

        return make_response(jsonify(data), 200)

    else:
        data['result'] = False
        return make_response(jsonify(data), 400)


@api_app.route('/create', methods=['POST'])
@check_allow
# @login_required
def create_view():
    data = dict()

    # get and check params
    params = request.get_json()
    port = params.get('port')
    if port is None:
        data['result'] = False
        data['message'] = 'Pass parameters'
        return make_response(jsonify(data), 400)

    # start docker container
    peer = Peer()
    result = peer.create(port)

    if result['returncode'] == 0:
        data['result'] = True
        data['container'] = result['data']
        data['port'] = port

        return make_response(jsonify(data), 200)

    else:
        data['result'] = False
        data['message'] = "Container haven't run"
        return make_response(jsonify(data), 400)


@api_app.route('/start/<int:port>', methods=['GET'])
@check_allow
# @login_required
def start_view(port):
    data = dict()

    # start docker container
    peer = Peer()
    result = peer.start(port)

    if result['returncode'] == 0:
        data['result'] = True
        data['container'] = result['data']
        data['port'] = port

        return make_response(jsonify(data), 200)

    else:
        data['result'] = False
        data['message'] = "Container haven't run"
        return make_response(jsonify(data), 400)


@api_app.route('/stop/<int:port>', methods=['GET'])
@check_allow
# @login_required
def stop_view(port):
    peer = Peer()
    result = peer.stop(port=port)

    data = dict()

    if result['returncode'] == 0:
        data['result'] = True
        data['container'] = result['data']
        data['port'] = port

        return make_response(jsonify(data), 200)

    else:
        data['result'] = False
        data['message'] = 'Container not found'

        return make_response(jsonify(data), 400)


@api_app.route('/remove/<int:port>', methods=['GET'])
@check_allow
# @login_required
def remove_view(port):
    peer = Peer()
    result = peer.remove(port=port)

    data = dict()

    if result['returncode'] == 0:
        data['result'] = True
        data['container'] = result['data']
        data['port'] = port

        return make_response(jsonify(data), 200)

    else:
        data['result'] = False
        data['message'] = 'Container not found or not stopped'

        return make_response(jsonify(data), 400)


@api_app.route('/remove_by_id/<string:id>', methods=['GET'])
@check_allow
# @login_required
def remove_by_id_view(id):
    peer = Peer()
    result = peer.remove_by_id(id=id)

    data = dict()

    if result['returncode'] == 0:
        data['result'] = True
        data['container'] = result['data']

        return make_response(jsonify(data), 200)

    else:
        data['result'] = False
        data['message'] = 'Container not found or not stopped'

        return make_response(jsonify(data), 400)


@api_app.route('/clear/<int:port>', methods=['GET'])
@check_allow
# @login_required
def clear_view(port):
    peer = Peer()
    result = peer.clear(port=port)
    stop, remove = result

    # format result
    data = dict()
    if stop['returncode'] == 0 and remove['returncode'] == 0:
        data['result'] = True
        data['port'] = port

        return make_response(jsonify(data), 200)

    else:
        data['result'] = False
        data['message'] = 'Container not found'
        return make_response(jsonify(data), 400)
