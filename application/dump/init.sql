DROP DATABASE IF EXISTS fastmeetpeer;
CREATE DATABASE fastmeetpeer;
CREATE USER fastmeetpeer  WITH LOGIN password 'fastmeetpeer';
GRANT ALL privileges ON DATABASE fastmeetpeer TO fastmeetpeer;

-- testing
DROP DATABASE IF EXISTS fastmeetpeer_test;
CREATE DATABASE fastmeetpeer_test;
GRANT ALL privileges ON DATABASE fastmeetpeer_test TO fastmeetpeer;
ALTER USER fastmeetpeer WITH CREATEDB;
