import sys
from os import path
# add parent path is source root
sys.path.append(path.abspath(path.curdir))


from application.app import create_app
from application.config import config


if __name__ == '__main__':
    application = create_app(autotest=False)
    application.run(
        host=config.SERVER['HOST'],
        port=config.SERVER['PORT'],
        debug=config.DEBUG,
        load_dotenv=True
    )
