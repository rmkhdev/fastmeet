"""
Декоратор check_allow - проверяет по токену и хосту на доступ
"""

from functools import wraps
from flask import request, Response
from application.quiries.tokens import query_get_allow


def check_allow(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        token = request.headers.get('Token')
        origin = request.headers.get('Host')

        if origin is None:
            return Response(status=403)

        if origin.find(':') == -1:
            origin = origin + ':80'

        host, port = origin.split(':')

        token = query_get_allow(host, port, token)
        if token is not None:
            result = func(*args, **kwargs)
            return result

        return Response(status=403)

    return wrapper
