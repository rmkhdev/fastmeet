from flask import Flask, request, Response, redirect, url_for
from application.config import config
from application.view import admin_app, api_app, pages_app
from flask_migrate import Migrate
from application.models import db
from flask_login import LoginManager
from datetime import timedelta
from application.quiries.users import query_get
from application.manage.commands import add_user
import click


def create_app(autotest=False):
    # application
    app = Flask(__name__)
    app.debug = config.DEBUG

    # change database name for auto-testing mode
    if autotest:
        config.DATABASE['NAME'] = config.DATABASE['NAME_TEST']

    # init settings
    app.config.update(
        SQLALCHEMY_DATABASE_URI=f"postgresql+psycopg2://"
                                f"{config.DATABASE['USER']}:{config.DATABASE['PASSWORD']}@"
                                f"{config.DATABASE['HOST']}:5432/{config.DATABASE['NAME']}",
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        SECRET_KEY=config.AUTH_SECRET_KEY,
        REMEMBER_COOKIE_HTTPONLY=True,
        PERMANENT_SESSION_LIFETIME=timedelta(days=config.AUTH_LIFETIME),
    )

    if autotest:
        # Для теста! Не включаем в общий config, так как приложение юзается с хоста, который отличается от SERVER_NAME
        app.config.update(
            SERVER_NAME=f"{config.SERVER['HOST']}:{config.SERVER['PORT']}",
        )

    # init authentication params
    login_manager = LoginManager(app)
    login_manager.session_protection = config.AUTH_PROTECTION
    login_manager.login_view = "admin.login"

    @login_manager.unauthorized_handler
    def unauthorized_handler():
        if request.blueprint == 'api':
            return Response(status=401)

        return redirect(url_for('admin.login'))

    @login_manager.user_loader
    def load_user(user_id):
        return query_get(user_id)

    # init database
    db.init_app(app)
    migrate = Migrate(app, db)

    # commands
    @app.cli.command("adduser")
    @click.argument("user", default='admin')
    @click.argument("psw")
    def command_add_user(user, psw):
        add_user(user, psw)

    # init routes
    app.register_blueprint(admin_app)
    app.register_blueprint(api_app)
    app.register_blueprint(pages_app)

    return app
