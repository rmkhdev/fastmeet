from application.quiries.users import query_insert
from werkzeug.security import generate_password_hash
from application.const.validate import MANAGE_USER_CREATE_DATA_LEN_MIN


def add_user(username, password):
    data = dict()
    data['username'] = username
    data['password'] = generate_password_hash(password)
    data['name'] = username

    try:
        if len(username) < MANAGE_USER_CREATE_DATA_LEN_MIN or len(password) < MANAGE_USER_CREATE_DATA_LEN_MIN:
            raise ValueError('Not correct data (need more than 4 character)')

        result = query_insert(data)
        print('success:', result)

    except Exception as error:
        print('error:', error)
