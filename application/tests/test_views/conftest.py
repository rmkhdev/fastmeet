from application.quiries.users import query_insert as query_insert_user
from application.quiries.tokens import query_insert as query_insert_token
from werkzeug.security import generate_password_hash
from application.app import create_app
from application.models import db, User, Tokens
from flask import url_for


# create application in test mode
application = create_app(autotest=True)
application.app_context().push()

# migrations
with application.app_context():
    db.drop_all(app=application)
    db.create_all(app=application)


# run before each test
def pytest_runtest_setup():
    add_user()
    add_tokens()

    return None


# run after each test
def pytest_runtest_teardown():
    with application.app_context():
        db.session.query(User).delete()
        db.session.query(Tokens).delete()
        db.session.commit()

    return None


def login(client):
    url = url_for('admin.login')
    data = {
        "username": 'test',
        "password": 'test',
    }

    # request
    client.post(url, data=data)


def logout(client):
    url = url_for('admin.logout')

    # request
    client.get(url)


"""
    ADD TEST DATA
"""
DATA_TOKENS = [
    {
        'host': '127.0.0.1',
        'port': '9000',
        'token': '8LzP3gu0ak',
        'status': 1
    },
    {
        'host': '127.0.0.2',
        'port': '90002',
        'token': '8LzP3gu0ak',
        'status': 1
    }
]


def add_user():
    with application.app_context():
        # user
        data = dict()
        data['username'] = 'test'
        data['password'] = generate_password_hash('test')
        data['name'] = 'Test User'

        try:
            query_insert_user(data)
        except Exception as error:
            print('error:', str(error))


def add_tokens():
    with application.app_context():
        try:
            for token in DATA_TOKENS:
                query_insert_token(token)

        except Exception as error:
            print('error:', str(error))
