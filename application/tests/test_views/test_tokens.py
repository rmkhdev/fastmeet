from pytest import fixture
from .conftest import application, DATA_TOKENS, login, logout
from flask import url_for


@fixture
def client():
    with application.test_client() as client:
        with application.app_context():
            login(client)
            yield client
            logout(client)


def test_tokens_list_status(client):
    url = url_for('admin.tokens_list')
    response = client.get(url)

    count = len(DATA_TOKENS)
    assert response.status_code == 200
    assert f'<div class="block" id="tokens-count">Total: {count}</div>' in response.data.decode("utf-8")


def test_tokens_active(client):
    url = url_for('admin.tokens_list')
    response = client.get(url)

    assert b'<a href="/tokens" class="headline__menu__item headline__menu__item_active">Tokens</a>' in response.data


def test_tokens_add_html(client):
    url = url_for('admin.tokens_add')
    response = client.get(url)

    assert b' <form action="/tokens/add" method="post" class="form">' in response.data
