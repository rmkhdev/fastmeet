from pytest import fixture
from .conftest import application, login, logout
from flask import url_for


@fixture
def client():
    with application.test_client() as client:
        with application.app_context():
            login(client)
            yield client
            logout(client)


def test_containers(client):
    url = url_for('admin.containers_list')
    response = client.get(url)

    assert response.status_code == 200
