from .base import BaseSettings


class LocalSettings(BaseSettings):
    DEBUG = True

    # FLASK WEB SERVER PARAMS
    SERVER = {
        'HOST': '0.0.0.0',
        'PORT': '4070',
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': '127.0.0.1',
        'NAME': 'fastmeetpeer',
        'NAME_TEST': 'fastmeetpeer',
        'USER': 'fastmeetpeer',
        'PASSWORD': 'fastmeetpeer',
    }
