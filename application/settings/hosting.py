from .base import BaseSettings


class HostingSettings(BaseSettings):
    DEBUG = False

    # FLASK WEB SERVER PARAMS
    SERVER = {
        'HOST': '0.0.0.0',
        'PORT': '80',
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': '45.84.225.128',
        'NAME': 'fastmeetpeer',
        'NAME_TEST': 'fastmeetpeer',
        'USER': 'fastmeetpeer',
        'PASSWORD': 'fastmeetpeer',
    }
