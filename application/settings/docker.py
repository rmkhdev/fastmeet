from .base import BaseSettings


class DockerSettings(BaseSettings):
    DEBUG = False

    # FLASK WEB SERVER PARAMS
    SERVER = {
        'HOST': '0.0.0.0',
        'PORT': '80',
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': 'fastmeetpeerdb',
        'NAME': 'fastmeetpeer',
        'NAME_TEST': 'fastmeetpeer',
        'USER': 'fastmeetpeer',
        'PASSWORD': 'fastmeetpeer',
    }
