from .base import BaseSettings


class ProductionSettings(BaseSettings):
    DEBUG = False

    # FLASK WEB SERVER PARAMS
    SERVER = {
        'HOST': '0.0.0.0',
        'PORT': '80',
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': '192.168.43.211',
        'NAME': 'fastmeetpeer',
        'NAME_TEST': 'fastmeetpeer',
        'USER': 'fastmeetpeer',
        'PASSWORD': 'fastmeetpeer',
    }
