from .base import BaseSettings


class CiSettings(BaseSettings):
    # flask web server host
    SERVER = {
        'HOST': '127.0.0.1',
        'PORT': '80',
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': 'postgres',
        'NAME': 'postgres',
        'NAME_TEST': 'fastmeetpeer',
        'USER': 'fastmeetpeer',
        'PASSWORD': 'fastmeetpeer',
    }
