import os
from os import getenv
from os import path
from os.path import join, dirname
from dotenv import load_dotenv
from application.settings.base import BaseSettings
from application.settings.production import ProductionSettings
from application.settings.ci import CiSettings
from application.settings.local import LocalSettings
from application.settings.docker import DockerSettings
from application.settings.hosting import HostingSettings


# GET PARAMS FROM ENV FILE
env_file = path.abspath(path.curdir) + '/database.env'
load_dotenv(env_file)

# get environ
env = os.environ.get('env')

# get configs by environ
if env is None:
    config = BaseSettings()
elif env == 'production':
    config = ProductionSettings()
elif env == 'docker':
    config = DockerSettings()
elif env == 'local':
    config = LocalSettings()
elif env == 'hosting':
    config = HostingSettings()
elif env == 'ci':
    config = CiSettings()
else:
    raise ValueError("Environment name isn't specified")

# override from env
POSTGRES_HOST = getenv('POSTGRES_HOST')
if POSTGRES_HOST is not None:
    config.DATABASE['HOST'] = POSTGRES_HOST

POSTGRES_DB = getenv('POSTGRES_DB')
if POSTGRES_DB is not None:
    config.DATABASE['NAME'] = POSTGRES_DB

POSTGRES_USER = getenv('POSTGRES_USER')
if POSTGRES_USER is not None:
    config.DATABASE['USER'] = POSTGRES_USER

POSTGRES_PASSWORD = getenv('POSTGRES_PASSWORD')
if POSTGRES_PASSWORD is not None:
    config.DATABASE['PASSWORD'] = POSTGRES_PASSWORD
